import tweepy
import pandas as pd
import config


# function to display data of each tweet
def printtweetdata(n, ith_tweet):
        print(retweets)
        print()
        print(f"Tweet {n}:")
        print(f"Date: {ith_tweet[0]}")
        print(f"Username: {ith_tweet[1]}")
        print(f"Tweet Text:{ith_tweet[2]}")
        print(f"Likes: {ith_tweet[3]}")
 
 
# function to perform data extraction
def scrape(words, date_since, numtweet, file_name):
 
        # Creating DataFrame using pandas
        db = pd.DataFrame(columns=['date','username','text','likes'])
 
        # We are using .Cursor() to search
        # through twitter for the required tweets.
        # The number of tweets can be
        # restricted using .items(number of tweets)
        tweets = tweepy.Cursor(api.search_tweets,
                               words, lang="en",
                               since_id=date_since,
                               tweet_mode='extended').items(numtweet)
 
 
        # .Cursor() returns an iterable object. Each item in
        # the iterator has various attributes
        # that you can access to
        # get information about each tweet
        list_tweets = [tweet for tweet in tweets]
 
        # Counter to maintain Tweet Count
        i = 1
 
        # we will iterate over each tweet in the
        # list for extracting information about each tweet
        for tweet in list_tweets:
                username = tweet.user.screen_name
                date = tweet.created_at
                likes = tweet.favorite_count
                text = tweet.full_text
                
                # checks to see if the tweet is a 
                # retweet. If it is, it's skipped
                if hasattr(tweet, "retweeted_status"):
                        if retweets == False:
                                continue
                        elif retweets == True:
                                pass
                        else:
                                pass

                else:        
                        # Here we are appending all the
                        # extracted information in the DataFrame
                        ith_tweet = [date, username, text, likes]
                        db.loc[len(db)] = ith_tweet
        
                        # Function call to print tweet data on screen
                        printtweetdata(i, ith_tweet)
                        i = i+1
 
        # we will save our database as a CSV file.
        db.to_csv(file_name)
 
if __name__ == '__main__':
 
    auth = tweepy.OAuth1UserHandler(config.consumer_key, config.consumer_secret, config.access_token, config.access_token_secret
)

    api = tweepy.API(auth)
 
        # Enter Hashtag and initial date
    word = input("Enter Twitter HashTag to search for: ")
    words = "#" + word
    date_since = input("Enter Date since The Tweets are required in yyyy-mm--dd: ")
    file_name = input("Please choose the name of the csv to save tweets to. Use: 'name.csv': ")
    strtweet = input("How many tweets would you like to limit it to?: ") 
    numtweet = int(strtweet)
    retweet = input("Would you like to skip retweets? 1 for yes, 0 for no: ")
    retweets = bool(retweet)


    scrape(words, date_since, numtweet, file_name)
    print('Scraping has completed!')
